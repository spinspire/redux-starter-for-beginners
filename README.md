# Redux-Starter-For-Beginners

## Commands
- First run `npm install`
- For hot-reloading and development `npm run dev` and visit localhost:3000
- For production compile `npm run build`

## Steps
These steps are embedded in the code. See index.js and App.js
- Application global steps in index.js
- Per-Component steps in App.js
