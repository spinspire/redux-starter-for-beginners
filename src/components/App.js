import React from 'react'
import {connect} from 'react-redux'
import NameInput from './NameInput'
import Greeting from './Greeting'

class App extends React.Component {
    render() {
        const {guestName, changeGuestName} = this.props
        return (
            <div>
                <NameInput changeGuestName={changeGuestName} />
                <Greeting guestName={guestName} />
            </div>
        )
    }
}

// Redux-Per-Component 3: map callbacks (that use 'dispatch') to the component's prop keys
const mapDispatchToProps = (dispatch) => ({
    changeGuestName: (e) =>  dispatch({type: 'GUEST_NAME', value: e.target.value})
})

// Redux-Per-Component 2: map values from state to the component's prop keys
const mapStateToProps = (state) => ({guestName: state.guestName})

// PER CONNECT'D COMPONENT
// Redux-Per-Component 1: connect the component to the store
export default connect(mapStateToProps, mapDispatchToProps)(App)
