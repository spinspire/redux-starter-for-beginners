import React from 'react'

export default ({changeGuestName}) => <input onChange={changeGuestName} placeholder="Enter guest name"/>
