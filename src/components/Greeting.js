import React from 'react'

export default ({guestName}) => <h1>Hello {guestName ? guestName : 'John Doe'}!</h1>
