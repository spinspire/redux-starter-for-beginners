import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import {Provider} from 'react-redux' // Provider comes from react-redux
import {createStore, combineReducers} from 'redux' // createStore and combineReducers come from redux (core)

// Redux-Global 4: Create several reducers, each managing a section of the store.
// NOTE that the first argument is NOT the entire store, just a section of it.
const guestNameReducer = (guestName = null, action) => {
  switch(action.type) {
    case 'GUEST_NAME':
      return action.value
    break;
  }
  return guestName
}

// Redux-Global 3: Combine all your reducers into one. Each key represents
// one section of the store and receives its reducer
const rootReducer = combineReducers({
  guestName: guestNameReducer, // the key is important; it is they key in the store
  // more reducers go here under different store keys
})

// Redux-Global 2: create a single store for the whole app
const store = createStore(rootReducer)

// Redux-Global 1: wrap the whole app in Provider
ReactDOM.render(<Provider store={store}><App/></Provider>, document.querySelector('#main'));
